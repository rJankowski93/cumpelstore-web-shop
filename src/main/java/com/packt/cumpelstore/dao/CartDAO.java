package com.packt.cumpelstore.dao;

import com.packt.cumpelstore.data.CartData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CartDAO extends JpaRepository<CartData, Long> {
    CartData create(CartData cart);

    CartData read(String cartId);

    void update(String cartId, CartData cart);

    void delete(String cartId);
}
