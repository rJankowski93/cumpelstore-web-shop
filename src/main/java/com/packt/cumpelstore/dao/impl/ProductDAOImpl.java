package com.packt.cumpelstore.dao.impl;

import com.packt.cumpelstore.dao.AbstractDAO;
import com.packt.cumpelstore.data.ProductData;
import com.packt.cumpelstore.exception.ApplicationException;
import com.packt.cumpelstore.exception.DAOError;

import javax.persistence.criteria.*;
import java.util.List;
import java.util.Map;

public class ProductDAOImpl extends AbstractDAO<ProductData> {

    public ProductDAOImpl() {
        super(ProductData.class);
    }

    //TODO add sorting ang pagination
    public List<ProductData> getItemsByCategory(String category) {
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<ProductData> cq = cb.createQuery(ProductData.class);
        Root<ProductData> productData = cq.from(ProductData.class);
        Expression<String> name = productData.get("category");
        Predicate eq1 = cb.equal(name, category);
        cq.select(productData).where(eq1);
        List<ProductData> productsByCategory = getEntityManager().createQuery(cq).getResultList();
        if (productsByCategory == null || productsByCategory.isEmpty()) {
            throw new ApplicationException(DAOError.NO_PRODUCT_CATEGORY, category);
        }
        return productsByCategory;
    }

    //TODO add sorting ang pagination
    public List<ProductData> getProductsByFilter(Map<String, List<Object>> filterParams) {
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<ProductData> cq = cb.createQuery(ProductData.class);
        Root<ProductData> r = cq.from(ProductData.class);
        Predicate predicateAnd = cb.conjunction();
        Predicate predicateOr = null;
        for (Map.Entry<String, List<Object>> param : filterParams.entrySet()) {
            for (Object o : param.getValue()) {
                if (o == param.getValue().get(0)) {
                    if (param.getKey().equals("priceFrom")) {
                        predicateOr = cb.greaterThanOrEqualTo((Expression) r.get("price"), Long.parseLong((String) o));
                    } else if (param.getKey().equals("priceTo")) {
                        predicateOr = cb.lessThanOrEqualTo((Expression) r.get("price"), Long.parseLong((String) o));
                    } else {
                        predicateOr = cb.equal(r.get(param.getKey()), o);
                    }

                } else {
                    predicateOr = cb.or(predicateOr, cb.equal(r.get(param.getKey()), o));
                }
            }
            predicateAnd = cb.and(predicateAnd, predicateOr);
        }
        cq.where(predicateAnd);
        return getEntityManager().createQuery(cq).getResultList();
    }

    //TODO add sorting ang pagination
    public List<ProductData> getPromotionProductsList() {
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<ProductData> cq = cb.createQuery(ProductData.class);
        Root<ProductData> item = cq.from(ProductData.class);
        cq.select(item).where(cb.equal(item.get("isPromotion"), true));
        List<ProductData> products = getEntityManager().createQuery(cq).getResultList();
        if (products == null || products.isEmpty()) {
            throw new ApplicationException(DAOError.NO_PRODUCT_PROMOTION);
        }
        return products;
    }

    public List<ProductData> getProductByCode(String code) {
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<ProductData> cq = cb.createQuery(ProductData.class);
        Root<ProductData> productData = cq.from(ProductData.class);
        Expression<String> fieldName = productData.get("code");
        Predicate eq1 = cb.equal(fieldName, code);
        cq.select(productData).where(eq1);
        List<ProductData> products = getEntityManager().createQuery(cq).getResultList();
        if (products == null || products.isEmpty()) {
            throw new ApplicationException(DAOError.NO_PRODUCT_CODE);
        }
        return products;
    }
}
