package com.packt.cumpelstore.dao;

import com.packt.cumpelstore.data.OrderData;
import com.packt.cumpelstore.data.PayPalData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderDAO extends JpaRepository<OrderData, Long> {
    void createItem(OrderData orderData);

    List<OrderData> getItemsByState(String state);

    PayPalData getPayPalConfig();

    List<OrderData> getList(Integer offset, Integer maxResults, String sortBy);

    Long countQuantity();
}
