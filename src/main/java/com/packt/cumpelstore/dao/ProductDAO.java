package com.packt.cumpelstore.dao;

import com.packt.cumpelstore.data.ProductData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public interface ProductDAO extends JpaRepository<ProductData, Long> {

    List<ProductData> getItemsByCategory(String category);

    List<ProductData> getProductsByFilter(Map<String, List<Object>> filterParams);

    List<ProductData> getPromotionProductsList();

    List<ProductData> getProductByCode(String code);

    List<ProductData> getList(Integer offset, Integer maxResults, String sortBy);

    Long countQuantity();
}
