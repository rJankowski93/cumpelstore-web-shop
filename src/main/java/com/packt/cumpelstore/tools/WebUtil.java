package com.packt.cumpelstore.tools;

import org.apache.log4j.Logger;
import org.apache.log4j.Priority;

import javax.servlet.http.HttpServletRequest;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class WebUtil {

    private static final Logger log = Logger.getLogger(WebUtil.class);

    public static String getIpAddr(HttpServletRequest request) {
        String ipAddress = request.getHeader("X-FORWARDED-FOR");
        if (ipAddress == null) {
            ipAddress = request.getRemoteAddr();
        }
        if (ipAddress.equalsIgnoreCase("0:0:0:0:0:0:0:1")) {
            InetAddress inetAddress;
            try {
                inetAddress = InetAddress.getLocalHost();
                ipAddress = inetAddress.getHostAddress();
            } catch (UnknownHostException e) {
                log.error("nie pobrano adresu IP",e);
            }
        }
        return ipAddress;
    }

}
