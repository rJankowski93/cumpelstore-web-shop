package com.packt.cumpelstore.tools;

import java.util.regex.Matcher;

public class StringUtil {

	public final static String replace(String pMessage, Object[] pParams) {
		if (pParams != null && pMessage != null) {
			for (int i = 0; i < pParams.length; i++) {
				pMessage = pMessage.replaceAll("\\{" + i + "\\}", ((pParams.length > i && pParams[i] != null)
						? Matcher.quoteReplacement(pParams[i].toString()) : "null"));
			}
		}
		return pMessage;
	}
}
