package com.packt.cumpelstore.controller;

import com.packt.cumpelstore.data.CustomerData;
import com.packt.cumpelstore.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.text.ParseException;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/customers")
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    private Long addressId;
    private Long userId;

    @RequestMapping()
    public String list(Model model, Integer offset, Integer maxResults, String orderBy) {
        model.addAttribute("customers", customerService.getList(offset, maxResults, orderBy));
        model.addAttribute("count", customerService.countQuantity());
        model.addAttribute("offset", offset);
        model.addAttribute("orderBy", orderBy);
        return "Customer/CustomersList";
    }

    @RequestMapping("/customer")
    public String getItemById(Model model, @RequestParam("id") Long id) {
        model.addAttribute("customer", customerService.getItemById(id));
        return "Customer/CustomerDetails";
    }

    @RequestMapping("/removeCustomer")
    public String removeCustomer(@RequestParam("id") Long id) {
        customerService.removeItem(id);
        return "redirect:/customers";
    }

    @RequestMapping(value = "/modifyCustomer", method = RequestMethod.GET)
    public String modifyCustomer(Model model, @RequestParam("id") Long id) {
        CustomerData customer = customerService.getItemById(id);
        model.addAttribute("customer", customer);
        addressId = customer.getAddressId();
        userId = customer.getUserId();
        return "Customer/CustomerModify";
    }

    @RequestMapping(value = "/modifyCustomer", method = RequestMethod.POST)
    public String saveModifiedCustomer(@ModelAttribute("customer") CustomerData customer) {
        customer.getAddress().setId(addressId);
        customer.getUser().setId(userId);
        customerService.saveItem(customer);
        return "redirect:/customers";
    }

    @RequestMapping(value = "/create", method = RequestMethod.GET)
    public String createCustomer(Model model) {
        CustomerData customer = new CustomerData();
        model.addAttribute("customer", customer);
        return "Customer/CustomerCreate";
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public String saveNewCustomer(@ModelAttribute("customer") @Valid CustomerData customer, BindingResult result) {
        if (result.hasErrors()) {
            return "Customer/CustomerCreate";
        }
        customerService.createItem(customer);
        return "redirect:/customers";
    }

    @RequestMapping("/filter/{ByCriteria}")
    public String getCustomerByFilter(@MatrixVariable(pathVar = "ByCriteria") Map<String, List<Object>> filterParams, Model model) throws ParseException {
        model.addAttribute("customers", customerService.getCustomerByFilter(filterParams));
        return "Customer/CustomersList";
    }

}
