package com.packt.cumpelstore.payment.payu.model;

import com.packt.cumpelstore.data.CartItemData;
import com.packt.cumpelstore.data.OrderData;
import com.packt.cumpelstore.data.ProductData;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class PayuData implements Serializable {
    private String customerIp;
    private int merchantPosId;
    private String description;
    private String notifyUrl;
    private String continueUrl;
    private String currencyCode = "PLN";
    private String openPayuSignature;
    private List<PayuProduct> products;

    public PayuData() {
    }

    public PayuData(String currencyCode) {
        setCurrencyCode(currencyCode);
    }

    public String getCustomerIp() {
        return customerIp;
    }

    public void setCustomerIp(String customerIp) {
        this.customerIp = customerIp;
    }

    public int getMerchantPosId() {
        return merchantPosId;
    }

    public void setMerchantPosId(int merchantPosId) {
        this.merchantPosId = merchantPosId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getTotalAmount() {
        long totalAmount = 0;
        for (PayuProduct product : products) {
            totalAmount += product.getUnitPrice() * product.getQuantity();
        }
        return totalAmount;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getNotifyUrl() {
        return notifyUrl;
    }

    public void setNotifyUrl(String notifyUrl) {
        this.notifyUrl = notifyUrl;
    }

    public String getContinueUrl() {
        return continueUrl;
    }

    public void setContinueUrl(String continueUrl) {
        this.continueUrl = continueUrl;
    }

    public String getOpenPayuSignature() {
        return openPayuSignature;
    }

    public void setOpenPayuSignature(String openPayuSignature) {
        this.openPayuSignature = openPayuSignature;
    }

    public List<PayuProduct> getProducts() {
        return products;
    }

    public void setProducts(OrderData order) {
        List<CartItemData> cartItems = order.getCart().getCartItems();
        products = new ArrayList<>();

        for (CartItemData cartItem : cartItems) {
            ProductData product = cartItem.getProduct();
            PayuProduct payuProduct = new PayuProduct(product.getName(), product.getPrice(), cartItem.getQuantity(), getCurrencyCode());
            products.add(payuProduct);
        }
    }

    @Override
    public String toString() {
        return ("customerIp: " + customerIp) +
                "merchantPosId: " + merchantPosId +
                "openPayuSignature: " + openPayuSignature;
    }
}
