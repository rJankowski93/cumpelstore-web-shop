package com.packt.cumpelstore.payment.payu.service;

import com.packt.cumpelstore.data.OrderData;
import com.packt.cumpelstore.payment.payu.utils.PayuUtils;
import com.packt.cumpelstore.payment.payu.model.PayuData;
import com.packt.cumpelstore.tools.WebUtil;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.webflow.execution.RequestContext;

import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;

@Service
public class PayuService {

    Logger logger = Logger.getLogger("auditLogger");

    @Value("${payu.pos_id}")
    private int posId;

    @Value("${payu.md5_key}")
    private String md5Key;

    @Value("${payu.notifyUrl}")
    private String notifyUrl;

    @Value("${payu.continueUrl}")
    private String continueUrl;

    public PayuData createPayuData(OrderData order, RequestContext context) throws UnsupportedEncodingException {
        HttpServletRequest request = (HttpServletRequest)context.getExternalContext().getNativeRequest();
        String ip = WebUtil.getIpAddr(request);
        //TODO walutę będzie trzeba jakoś przekazać
        String currency = "PLN";
        return PayuUtils.createPayuData(posId, md5Key, notifyUrl, continueUrl, currency, "opis zamówienia", order, ip);
    }

}
