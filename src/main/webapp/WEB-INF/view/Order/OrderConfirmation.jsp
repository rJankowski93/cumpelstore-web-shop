<%--
  Created by IntelliJ IDEA.
  User: Rafi
  Date: 01/09/2016
  Time: 20:15
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html">
    <link rel="stylesheet"
          href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
    <title>Order Confirmation</title>
</head>

<body>

<section>
    <div class="jumbotron">
        <div class="container">
            <h1>Order</h1>
            <p>Order Confirmation</p>
        </div>
    </div>
</section>
<div class="container">
    <div class="row">
        <form:form modelAttribute="order" class="form-horizontal">
            <input type="hidden" name="_flowExecutionKey"
                   value="${flowExecutionKey}"/>

            <div
                    class="well col-xs-10 col-sm-10 col-md-6 col-xs-offset-1 col-sm-offset-1 col-md-offset-3">
                <div class="text-center">
                    <h1>Receipt</h1>
                </div>
                <div class="row">
                    <div class="col-xs-6 col-sm-6 col-md-6">
                        <address>
                            <strong>Shipping Address</strong> <br>
                                ${order.customer.address.country}<br>
                                ${order.customer.address.city}
                            <br>
                                ${order.customer.address.street}
                            <br>
                                ${order.customer.address.houseNumber}
                            <br>
                        </address>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6 text-right">
                        <p>

                        </p>
                    </div>
                </div>

                <div class="row">

                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>Product</th>
                            <th>#</th>
                            <th class="text-center">Price</th>
                            <th class="text-center">Total</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach var="cartItem" items="${order.cart.cartItems}">
                            <tr>
                                <td class="col-md-9"><em>${cartItem.product.name}</em></td>
                                <td class="col-md-1" style="text-align: center">
                                        ${cartItem.quantity}</td>
                                <td class="col-md-1 text-center">$${cartItem.product.price}</td>
                                <td class="col-md-1 text-center">$${cartItem.totalPrice}</td>
                            </tr>
                        </c:forEach>

                        <tr>
                            <td> </td>
                            <td> </td>
                            <td class="text-right"><h4>
                                <strong>Grand Total: </strong>
                            </h4></td>
                            <td class="text-center text-danger"><h4>
                                <strong>$${order.cart.grandTotal}</strong>
                            </h4></td>
                        </tr>
                        </tbody>
                    </table>
                    <button id="back" class="btn btn-default"
                            name="_eventId_backToCollectShippingDetail">back
                    </button>

                    <button type="submit" class="btn btn-success"
                            name="_eventId_orderConfirmed">
                        Confirm   <span class="glyphicon glyphicon-chevron-right"></span>
                    </button>
                    <button id="btnCancel" class="btn btn-default"
                            name="_eventId_cancel">Cancel
                    </button>

                </div>
            </div>
        </form:form>
        <form method="post" action="${payPalConfig.posturl}">
            <input type="hidden" name="upload" value="1">
            <input type="hidden" name="return" value="${payPalConfig.returnurl}">
            <input type="hidden" name="cmd" value="${payPalConfig.cmd}">
            <input type="hidden" name="business" value="${payPalConfig.business}">
            <input type="hidden" name="charset" value="${payPalConfig.charset}">
            <input type="hidden" name="currency_code" value="${payPalConfig.currency_code}">

            <c:forEach var="cartItem" items="${order.cart.cartItems}" varStatus="loop">
                <input type="hidden" name="item_name_${loop.index+1}" value="${cartItem.product.name}">
                <input type="hidden" name="item_number_${loop.index+1}" value="${cartItem.product.code}">
                <input type="hidden" name="amount_${loop.index+1}" value="${cartItem.product.price}">
                <input type="hidden" name="quantity_${loop.index+1}" value="${cartItem.quantity}">
            </c:forEach>
            <input type="image" src="https://www.paypal.com/en_US/i/btn/btn_xpressCheckout.gif">
        </form>

        <form method="post" action="https://secure.payu.com/api/v2_1/orders">
            <input type="hidden" name="customerIp" value="${payu.customerIp}">
            <input type="hidden" name="merchantPosId" value="${payu.merchantPosId}">
            <input type="hidden" name="totalAmount" value="${payu.totalAmount}">
            <input type="hidden" name="description" value="${payu.description}">
            <input type="hidden" name="currencyCode" value="${payu.currencyCode}">
            <c:forEach var="product" items="${payu.products}" varStatus="loop">
                <input type="hidden" name="products[${loop.index}].name" value="${product.name}">
                <input type="hidden" name="products[${loop.index}].unitPrice" value="${product.unitPrice}">
                <input type="hidden" name="products[${loop.index}].quantity" value="${product.quantity}">
            </c:forEach>
            <input type="hidden" name="notifyUrl" value="${payu.notifyUrl}">
            <input type="hidden" name="continueUrl" value="${payu.continueUrl}">
            <input type="hidden" name="OpenPayu-Signature" value="${payu.openPayuSignature}">
            <button type="submit">Płacę z PayU</button>
        </form>
    </div>
</div>
</body>
</html>

