<%--
  Created by IntelliJ IDEA.
  User: Rafi
  Date: 24/07/2016
  Time: 12:16
  To change this template use File | Settings | File Templates.
--%>
<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
    <title><spring:message code="labels.order.modify"/></title>
</head>
<body>
<section>
    <div class="jumbotron">
        <div class="container">
            <h1>
                <spring:message code="labels.order.modify"/>
            </h1>
        </div>
    </div>
    <div class="pull-right" style="padding-right: 50px">
        <a href="?language=pl"> <spring:message code="labels.language.pl"/></a> | <a href="?language=en">
        <spring:message code="labels.language.en"/></a>
    </div>
</section>
<section class="container">
    <form:form modelAttribute="order" class="form-horizontal" enctype="multipart/form-data">
        <fieldset>
            <legend>
                <spring:message code="labels.order.create"/>
            </legend>


            <div class="form-group">
                <label class="control-label col-lg-2" > <spring:message code="labels.state"/>
                </label>
                <div class="col-lg-10">
                    <form:radiobutton path="state" value="Placed"/>
                    <spring:message code="labels.order.state.placed"/>
                    <form:radiobutton path="state" value="Paid"/>
                    <spring:message code="labels.order.state.paid"/>
                    <form:radiobutton path="state" value="Sent"/>
                    <spring:message code="labels.order.state.sent"/>
                    <form:radiobutton path="state" value="Finished"/>
                    <spring:message code="labels.order.state.finished"/>
                </div>
            </div>



            <div class="form-group">
                <div class="col-lg-offset-2 col-lg-10">
                    <input type="submit" id="btnAdd" class="btn btn-primary" value="Save"/>
                    <a class="btn btn-default" href="<spring:url value="/customers" />"> <span
                            class="glyphicon glyphicon-hand-left"></span>
                            <spring:message code="labels.return"/>
                </div>
            </div>

        </fieldset>
    </form:form>
</section>

</body>
</html>
