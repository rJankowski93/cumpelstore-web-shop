<%--
  Created by IntelliJ IDEA.
  User: Rafi
  Date: 24/07/2016
  Time: 12:15
  To change this template use File | Settings | File Templates.
--%>
<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="tag" uri="/WEB-INF/taglibs/customTaglib.tld" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
    <title><spring:message code="labels.orders.title" /></title>
</head>
<body>
<section>
    <div class="jumbotron">
        <div class="container">
            <h1>
                <spring:message code="labels.orders.title" />
            </h1>
            <p>
                <spring:message code="labels.order.list" />
            </p>
            <a href="<c:url value="/orders/create" />" class="btn btn-danger btn-mini"><spring:message
                    code="labels.order.create"/></a><br/>
            <a href="<c:url value="/" />" class="btn btn-danger btn-mini"><spring:message
                    code="labels.return"/></a><br/>
            <div class="pull-right" style="padding-right: 50px">
                <a href="?language=pl"> <spring:message code="labels.language.pl" /></a> | <a href="?language=en"> <spring:message code="labels.language.en" /></a>
            </div>
        </div>
    </div>
</section>


<section class="container col-md-2">
    <a href="<c:url value="/orders" />" class="btn btn-primary btn-mini btn-block"><spring:message code="labels.product.all"/></a>
    <a href="<c:url value="/orders/Placed" />" class="btn btn-info btn-mini btn-block"><spring:message code="labels.order.state.placed"/></a>
    <a href="<c:url value="/orders/Paid" />" class="btn btn-info btn-mini btn-block"><spring:message code="labels.order.state.paid"/></a>
    <a href="<c:url value="/orders/Sent" />" class="btn btn-info btn-mini btn-block"><spring:message code="labels.order.state.placed"/></a>
    <a href="<c:url value="/orders/Finished" />" class="btn btn-info btn-mini btn-block"><spring:message code="labels.order.state.placed"/></a>
</section>
<section class="container col-md-9">
    <div class="row">
        <c:forEach items="${orders}" var="order">
            <div class="thumbnail">
                <div class="caption">
                    <h3>${order.date}</h3>
                    <p class="label label-warning">${order.state}</p>
                    <p>${order.customer.firstname}</p>
                    <p>${order.customer.lastname}</p>
                    <p >${order.customer.user.login} </p>
                    <p>
                        <a href=" <spring:url value="/orders/order?id=${order.id}" /> " class="btn btn-primary"> <span class="glyphicon-info-sign glyphicon" /></span> <spring:message code="labels.details" /></a>
                        <a href=" <spring:url value="/orders/removeOrder?id=${order.id}" /> " class="btn btn-danger"> <span class="glyphicon-info-sign glyphicon" /></span> <spring:message code="labels.remove" /></a>
                        <a href=" <spring:url value="/orders/modifyOrder?id=${order.id}" /> " class="btn btn-danger"> <span class="glyphicon glyphicon-pencil glyphicon" /></span> <spring:message code="labels.modify" /></a>
                    </p>
                </div>
            </div>
        </c:forEach>
        <tag:paginate max="15" offset="${offset}" count="${count}"
                      uri="/orders" next="&raquo;" previous="&laquo;"/>
    </div>
</section>

</body>
</html>
