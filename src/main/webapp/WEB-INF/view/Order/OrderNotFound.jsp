<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
    <title><spring:message code="labels.error.title" /></title>
</head>
<body>
<section>
    <div class="jumbotron">
        <div class="container">
            <h3 class="alert alert-danger">${invalidOrder}</h3>
        </div>
    </div>
    <div class="pull-right" style="padding-right: 50px">
        <a href="?language=pl"> <spring:message code="labels.language.pl" /></a> | <a href="?language=en"> <spring:message code="labels.language.en" /></a>
    </div>
</section>

<section>
    <div class="container">
        <p>${url}</p>
        <p>${exception}</p>
    </div>

    <div class="container">
        <p>
            <a href="<spring:url value="/orders" />" class="btn btn-primary"> <span class="glyphicon-hand-left glyphicon"></span> <spring:message code="labels.order.list" />
            </a>
        </p>
    </div>

</section>
</body>
</html>
