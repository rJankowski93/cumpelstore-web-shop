<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link href="<c:url value="/resources/css/style.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">
    <script src="<c:url value="/resources/js/jquery.min.js" />"></script>
    <title><spring:message code="labels.error.title"/></title>
</head>
<body>
<section>
    <div class="jumbotron">
        <div class="container">
            <h3 class="alert alert-danger">${invalidProduct}</h3>
        </div>
    </div>
    <div class="pull-right" style="padding-right: 50px">
        <a href="?language=pl"> <spring:message code="labels.language.pl"/></a> | <a href="?language=en">
        <spring:message code="labels.language.en"/></a>
    </div>
</section>

<section>
    <div class="container">
        <p>${url}</p>
        <p>${exception}</p>
    </div>

    <div class="container">
        <p>
            <a href="<spring:url value="/products" />" class="btn btn-primary"> <span
                    class="glyphicon-hand-left glyphicon"></span> <spring:message code="labels.product.list"/>
            </a>
        </p>
    </div>

</section>
</body>
</html>
