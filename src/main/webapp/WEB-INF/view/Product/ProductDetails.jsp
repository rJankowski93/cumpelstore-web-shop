<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <link href="<c:url value="/resources/css/style.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">
    <script src="<c:url value="/resources/js/jquery.min.js" />"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.0.1/angular.min.js"></script>

    <script src="/resources/js/cartController.js"></script>
    <title><spring:message code="labels.product.title"/></title>
</head>
<body>
<section>
    <div class="jumbotron">
        <div class="container">
            <h1>
                <spring:message code="labels.product.title"/>
            </h1>
        </div>
    </div>
    <div class="pull-right" style="padding-right: 50px">
        <a href="?language=pl"> <spring:message code="labels.language.pl"/></a> | <a href="?language=en">
        <spring:message code="labels.language.en"/></a>
    </div>
</section>
<section class="container" ng-app="cartApp">
    {{2+2}}
    <div class="row">
        <div class="col-md-5">
            <img src="<c:url value="/resources/images/${product.code}.png"></c:url>" alt="image" style="width: 50%"/>
        </div>
        <div class="col-md-5">
            <h3>${product.name}</h3>
            <p>${product.description}</p>
            <p>
                <strong><spring:message code="labels.code"/> </strong><span
                    class="label label-warning">${product.code}</span>
            </p>
            <p>
                <strong><spring:message code="labels.manufacturer"/></strong>: ${product.manufacturer}
            </p>
            <p>
                <strong><spring:message code="labels.category"/></strong>: ${product.category}
            </p>
            <p>
                <strong><spring:message code="labels.product.count.stock"/></strong>: ${product.unitsInStock}
            </p>
            <h4>${product.price}PLN</h4>
            <p>
            <p ng-controller="cartCtrl">
                {{2+2}}
                {{${product.id}}}
                <a href="#" class="btn btn-warning btn-large" ng-click="addToCart('${product.id}')">
                    <span class="glyphicon-shopping-cart glyphicon"></span> Zamów teraz </a>
                <a href="<spring:url value="/cart" />" class="btn btn-default">
                    <span class="glyphicon-hand-right glyphicon"></span> Koszyk
                </a>


                <a href="#" class="btn btn-warning btn-large"> <span
                        class="glyphicon-shopping-cartData glyphicon"></span>
                    <spring:message code="labels.order"/> </a>
                <a class="btn btn-default" href="<spring:url value="/products" />"> <span class="glyphicon glyphicon-hand-left"></span> <spring:message code="labels.return"/>
            </a>
            </p>
        </div>
    </div>
</section>
</body>
</html>