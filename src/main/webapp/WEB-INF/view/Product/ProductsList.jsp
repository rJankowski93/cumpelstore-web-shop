<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="tag" uri="/WEB-INF/taglibs/customTaglib.tld" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
    <link href="<c:url value="/resources/css/style.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">
    <script src="<c:url value="/resources/js/jquery.min.js" />"></script>
    <script src="<c:url value="/resources/js/productsScripts.js" />"></script>

    <title><spring:message code="labels.products.title"/></title>

    <script type="text/javascript">
        window.addEventListener("load", function () {
            var valueSortBy = String('${orderBy}');
            if (valueSortBy && 0 != valueSortBy.length) {
                $('[name="orderBy"]').val(valueSortBy);
            }
        }, false);
    </script>
</head>
<body onload="setParametrSeachCriteria()">
<jsp:include page="PruductMenu.jsp" flush="true"/>
<section class="container col-md-2">
    <a href="<c:url value="/products" />" class="btn btn-primary btn-mini btn-block"><spring:message
            code="labels.product.all"/></a>
    <a href="<c:url value="/products/filter/category=Laptop" />" class="btn btn-info btn-mini btn-block"><spring:message
            code="labels.product.laptop"/></a>
    <a href="<c:url value="/products/filter/category=Tablet" />" class="btn btn-info btn-mini btn-block"><spring:message
            code="labels.product.tablet"/></a>
    <a href="<c:url value="/products/filter/category=Phone" />" class="btn btn-info btn-mini btn-block"><spring:message
            code="labels.product.phone"/></a>
    <a href="<c:url value="/products/filter/category=Monitor" />"
       class="btn btn-info btn-mini btn-block"><spring:message
            code="labels.product.monitor"/></a>
</section>
<section class="container col-md-9">
    <div class="container">
        <span class="col-md-2">Name</span>
        <input class="col-md-2" type="text" id="name">
        <span class="col-md-2">Category</span>
        <input class="col-md-2" type="text" id="category">
        <span class="col-md-2">Manufacturer</span>
        <input class="col-md-2" type="text" id="manufacturer">
        <span class="col-md-2">State</span>
        <input class="col-md-2" type="text" id="state">
        <span class="col-md-2">Price From</span>
        <input class="col-md-2" type="text" id="priceFrom">
        <span class="col-md-2">Price To</span>
        <input class="col-md-2" type="text" id="priceTo">
        &nbsp;
        <button class="col-md-2 col-md-offset-5" onclick="findProductsList()">Find</button>
        &nbsp;
    </div>

    <div class="row">
        <select name="orderBy" onchange="insertParamToUrl(this)">
            <option value="nameASC"><spring:message code="labels.product.sorting.nameASC"/></option>
            <option value="nameDESC"><spring:message code="labels.product.sorting.nameDESC"/></option>
            <option value="priceASC"><spring:message code="labels.product.sorting.priceASC"/></option>
            <option value="priceDESC"><spring:message code="labels.product.sorting.priceDESC"/></option>
        </select>
        <c:forEach items="${products}" var="product">
            <div class="thumbnail">
                <img alt="image" src="<c:url value="/resources/images/${product.code}.png"></c:url>" width="22%"
                     align="left">

                <div class="caption">
                    <h3>${product.name}</h3>
                    <p>${product.description}</p>
                    <p>${product.price}PLN</p>
                    <p>
                        <spring:message code="labels.product.count.stock"/>
                            ${product.unitsInStock}
                    </p>
                    <p>
                        <a href=" <spring:url value="/products/product?id=${product.id}" /> " class="btn btn-primary">
                            <span class="glyphicon-info-sign glyphicon"/></span> <spring:message code="labels.details"/></a>
                        <a href=" <spring:url value="/products/removeProduct?id=${product.id}" /> "
                           class="btn btn-danger"> <span class="glyphicon-info-sign glyphicon"/></span> <spring:message
                                code="labels.remove"/></a>
                        <a href=" <spring:url value="/products/modifyProduct?id=${product.id}" /> "
                           class="btn btn-danger"> <span class="glyphicon glyphicon-pencil glyphicon"/></span>
                            <spring:message code="labels.modify"/></a>
                    </p>
                </div>
            </div>
        </c:forEach>
        <tag:paginate max="15" offset="${offset}" count="${count}" orderBy="${orderBy}"
                      uri="/products/" next="&raquo;" previous="&laquo;"/>
    </div>
</section>
</body>
</html>
