package com.packt.cumpelstore.payment.payu;

import com.packt.cumpelstore.data.*;
import com.packt.cumpelstore.payment.payu.model.PayuData;
import com.packt.cumpelstore.payment.payu.utils.PayuSignature;
import com.packt.cumpelstore.payment.payu.utils.PayuUtils;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

public class PayuTest {
    private String md5Key = "13a980d4f851f3d9a1cfc792fb1f5e50";
    private Map<String, String> inputs = new HashMap<>();
    private PayuData payu;

    @Before
    public void setUp() {
        initInputParameters();
        initExamplePayu();
    }

    private void initExamplePayu() {
        payu = new PayuData("PLN");
        payu.setCustomerIp("93.93.93.93");
        payu.setMerchantPosId(12345);
    }

    private void initInputParameters() {
        inputs.put("customerIp", "123.123.123.123");
        inputs.put("merchantPosId", "145227");
        inputs.put("description", "Opis zamówienia");
        inputs.put("totalAmount", "1000");
        inputs.put("currencyCode", "PLN");
        inputs.put("products[0].name", "Produkt 1");
        inputs.put("products[0].unitPrice", "1000");
        inputs.put("products[0].quantity", "1");
        inputs.put("notifyUrl", "http://shop.url/notify");
        inputs.put("continueUrl", "http://shop.url/continue");
    }

    @Test
    public void calculateSig() throws Exception {
        String signature = PayuSignature.calculateSig(inputs, md5Key, PayuSignature.SHA_ALGORITHM);
        String expectedSignature = "565f9f4dda43c8e24ccab4472133d680e2aa58e1f58bea845c4cf2926965144d";
        assertEquals(expectedSignature, signature);
    }

    @Test
    public void hash() throws Exception {
        String message = PayuSignature.buildMessage(inputs, md5Key);
        String signature = PayuSignature.hash(message, PayuSignature.SHA_ALGORITHM);
        String expectedSignature = "565f9f4dda43c8e24ccab4472133d680e2aa58e1f58bea845c4cf2926965144d";
        assertEquals(expectedSignature, signature);
    }

    @Test
    public void buildMessage() throws Exception {
        String message = PayuSignature.buildMessage(inputs, md5Key);
        String excpectedMessage = "continueUrl=http%3A%2F%2Fshop.url%2Fcontinue&currencyCode=PLN&customerIp=123.123.123.123&description=Opis+zam%C3%B3wienia&merchantPosId=145227&notifyUrl=http%3A%2F%2Fshop.url%2Fnotify&products[0].name=Produkt+1&products[0].quantity=1&products[0].unitPrice=1000&totalAmount=1000&13a980d4f851f3d9a1cfc792fb1f5e50";
        assertEquals(excpectedMessage, message);
    }

    @Test
    public void payuDataToMap() throws Exception {
        ProductData product = new ProductData("21223","prod1",100L);
        OrderData order = createOrder(product, 1);
        payu.setProducts(order);

        Map<String, String> payuMap = PayuUtils.payuDataToMap(payu);
        assertEquals("prod1", payuMap.get("products[0].name"));
    }

    @Test
    public void testPayuProductsPrice() throws Exception {
        ProductData product = new ProductData("21223","prod1",200L);
        OrderData order = createOrder(product, 2);
        payu.setProducts(order);

        assertEquals(40000L,payu.getTotalAmount());
    }

    private OrderData createOrder(ProductData product, long quantity) {
        CartItemData item = new CartItemData();
        item.setProduct(product);
        item.setQuantity(quantity);
        List<CartItemData> cartItems = new ArrayList<>();
        cartItems.add(item);

        CartData cart = new CartData();
        cart.setCartItems(cartItems);
        OrderData order = new OrderData();
        order.setId(1L);
        order.setCart(cart);
        return order;
    }
}